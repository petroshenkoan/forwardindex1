
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
/**
 * Created by 1 on 30.07.2017.
 */
public class ForwardIndex {

    private static Map<String, List<Integer>> forwardIndex = new TreeMap<>();

    /**
     * Здесь мы проверяем, не вносили ли мы ранее слово в индекс,
     * если вносили, то просто добавляем в список позиций этого слова в тексте новую позицию.
     * Если нет, то мы создаем новый список позиций, добавляем в него полученную нами позицию,
     * и вместе со словом добавляем его как новый элемент индекса.
     */
    public static void setValue (String var, int value) {
        if (forwardIndex.containsKey(var)) {
            forwardIndex.get(var).add(value);
        } else {
            List<Integer> newList = new ArrayList<>();
            newList.add(value);
            forwardIndex.put(var, newList);
        }
    }

    public static void main() throws IOException {

        /**
         * читаем текст из файла
         */
        String text = new String(Files.readAllBytes(Paths.get(new File("E:\\SomeDir\\1.txt").toURI())));

        /**
         *  удаляем все пуктуационный символы и пробелы :
         * сначала мы находим все пунктуационыые символы с помощью регулярного выражения и заменяем их на пустые строки,
         * далее приводим все к нижнему регистру и разделяем на массив строк по пробелам
         */
        List<String> words = Arrays.asList(
                text
                        .replaceAll("[^a-zA-Zа-яА-Я1-9-]", " ")
                        .toLowerCase()
                        .split("\\s+")
        );

        /**
         * Проходим по всем словам текста и добавляем их в индекс
         */
        for (int number = 0; number < words.size(); number++) {
            setValue(words.get(number), number);
        }


        for (String word : forwardIndex.keySet()) {
            System.out.println(word);
            for (Integer position : forwardIndex.get(word)) {
                System.out.println(position);
            }
        }

    }
}
